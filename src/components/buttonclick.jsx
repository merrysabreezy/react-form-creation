import React, { Component } from "react";
class Button extends Component {
  constructor(props) {
    super(props);
    this.state = { name: "MerishaM" };
  }
  clickHandler = () => {
    this.setState({ name: "MerishaP" });
  };
  render() {
    return (
      <React.Fragment>
        <label> {this.state.name}</label>
        <button onClick={this.clickHandler}> AfterM,IwillBe</button>
      </React.Fragment>
    );
  }
}

export default Button;
