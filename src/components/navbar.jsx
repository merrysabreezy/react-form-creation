import React, { Component } from "react";
import logo from "./bluelogo.png";

class NavBar extends Component {
  render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-lg navbar navbar-dark bg-dark">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <a class="navbar-brand" href="register">
              <img src={logo} height="50" width="60" alt="white logo" />
            </a>
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <a className="nav-link" href="#">
                  Overview{" "}
                </a>
              </li>
              <li className="nav-item dropdown active">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Get Started
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a className="dropdown-item" href="">
                    Login
                  </a>
                  <a className="dropdown-item" href="">
                    Register
                  </a>
                </div>
              </li>
              <li className="nav-item active">
                <a className="nav-link" href="">
                  Verify{" "}
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}

export default NavBar;
