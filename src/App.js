import React, { Component } from "react";
import "./App.css";
import NavBar from "./components/navbar";
import Form from "./components/form";
import Clock from "./components/Clock";
import Footer from "./components/footer";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <Form />
        <Clock />
      </React.Fragment>
    );
  }
}

export default App;
