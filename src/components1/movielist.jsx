import React from "react";
import axios from "axios";
import "./style.css";
export default class MovieList extends React.Component {
  state = {
    movies: []
  };
  componentDidMount() {
    axios
      .get("https://yts.am/api/v2/list_movies.json", {
        params: {
          quality: "720p",
          minimum_rating: "7.0",
          genre: "comedy",
          sort_by: "year"
        }
      })
      .then(response => {
        const movies = response.data.data.movies;
        console.log(movies);
        this.setState({ movies });
      });
  }
  render() {
    return (
      <React.Fragment>
        <table>
          <tr>
            <th> Id</th>
            <th> Title</th>
            <th> Rating</th>
            <th> Year </th>
            <th> Genre </th>
            <th> Quality</th>
          </tr>
          {this.state.movies.map(movie => (
            <tr>
              <td>{movie.id}</td>
              <td>{movie.title}</td>
              <td> {movie.rating} </td>
              <td> {movie.year} </td>
              <td> {movie.genres} </td>
              <td> {movie.torrents.seed}</td>
            </tr>
          ))}
        </table>
      </React.Fragment>
    );
  }
}
