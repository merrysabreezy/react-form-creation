import React from "react";
import axios from "axios";
export default class Weather extends React.Component {
  state = {
    weather: []
  };
  componentDidMount() {
    axios
      .get(
        "http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=fbaceafbb0ce6e665c51c648c63e273b"
      )
      .then(response => {
        const weather = response.data.list;
        console.log(weather);
        this.setState({ weather });
      });
  }
  render() {
    return (
      <ul>
        <tr>
          <td> Weather</td> <br />
          <td> Temperature</td> <br />
          <td> Humidity</td>
        </tr>
        {this.state.weather.map(weather => (
          <React.Fragment>
            <tr>
              <td>{weather.weather.description}</td> <br />
              <td> {weather.main.temp} </td> <br />
              <td>{weather.main.humidity}</td> <br />
            </tr>
          </React.Fragment>
        ))}
      </ul>
    );
  }
}
